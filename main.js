var url =
  "https://api.digitalslidearchive.org/api/v1/item/5b9ef9c8e62914002e94771e/tiles/zxy/{z}/{x}/{y}?edge=crop";

// minimap

const overviewMapControl = new ol.control.OverviewMap({
  // see in overviewmap-custom.html to see the custom CSS used
  className: "ol-overviewmap ol-custom-overviewmap",
  layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM({
        url: url,
      }),
    }),
  ],
  collapseLabel: "\u00BB",
  label: "\u00AB",
  collapsed: false,
});

const map = new ol.Map({
  controls: ol.control.defaults().extend([overviewMapControl]),
  layers: [
    new ol.layer.Tile({
      source: new ol.source.XYZ({
        url: url,
      }),
    }),
  ],
  target: "map",
  view: new ol.View({
    center: [40, 80],
    zoom: 2,
    constrainRotation: 16,
  }),
});
